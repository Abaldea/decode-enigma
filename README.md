# decode enigma

Juego para escape room 2019 "Código Enigma".
El programa tiene una clave que el jugador debe descifrar con la ayuda de las pistas de la escape room (en físico).

El sw espera que se introduzca una posible clave y luego informa de coincidencias con la contraseña correcta:
*  Número de caracteres que coinciden con los de la contraseña, estén o no en la posición correcta.
*  Número de caracteres en posición correcta.

Si se intruduce la contraseña correcta mostrará por pantalla la siguiente pista para los jugadores de la escape room, un número (en formato ascii para que mantenga la estética del SW)

Tanto si hacierta como se se equivoca aparecerá por pantalla una sere de caracteres aleatorios simulando que se está decodificando algún documento.