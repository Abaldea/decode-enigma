#!/usr/bin/python3.5

import os
import random
import string
import time
import sys
import shutil
import random

def split_code(word):
    return [char for char in str(word)]

def success_print():
    chars = string.ascii_letters + string.digits
    for i in range (8000):
        sys.stdout.write(''.join(random.choice(chars)))
        sys.stdout.flush()
        time.sleep(0.0001)
    print('\n\n')

    columns = shutil.get_terminal_size().columns
    print('#######################################################'.center(columns))
    print('###########           CÓDIGO CORRECTO       ###########'.center(columns))
    print('#######################################################'.center(columns))
    print('\n\n')
    print('ENVIANDO ARCHIVOS>> ', end="")
    for i in range (50):
        print(".",end="")
        time.sleep(0.1)
        sys.stdout.flush()
    print(" OK")

    print('RECIVIENDO PATRÓN ANÓMALO>> ', end="")
    for i in range (42):
        print(".",end="")
        time.sleep(0.1)
        sys.stdout.flush()
    print(" OK")
    print()
    os.system("cat text.txt")

def failure_print(in_pwd, in_cp):
    chars = string.ascii_letters + string.digits
    for i in range (200):
        for j in range(random.randint(1,100)):
            sys.stdout.write(''.join(random.choice(chars)))
            sys.stdout.flush()
            time.sleep(0.0001)
        for j in range(random.randint(1,10)):
            sys.stdout.write('\x1b[1;31m' + ''.join(random.choice(chars)) + '\x1b[0m')
            sys.stdout.flush()
            time.sleep(0.0001)

    columns = shutil.get_terminal_size().columns
    print()
    print()
    print(('\x1b[1;31m' +'#######################################################'+ '\x1b[0m').center(columns))
    print(('\x1b[1;31m' +'###########           CÓDIGO ERRONEO        ###########'+ '\x1b[0m').center(columns))
    print(('\x1b[1;31m' +'#######################################################'+ '\x1b[0m').center(columns))
    print(('\x1b[1;31m' +'######## Caracteres en pwd: {}, en posición: {} #########'.format(in_pwd, in_cp)+'\x1b[0m').center(columns))
    print(('\x1b[1;31m' +'#######################################################'+ '\x1b[0m').center(columns))
    print('\n\n')

# CLAVE: 12345
# CODIG: 11223  PISTAS: caracteres en pwd 3, caracteres en posición correcta: 1
# CODIG: 54321  PISTAS: caracteres en pwd 5, caracteres en posición correcta: 1
# CODIG: 54213  PISTAS: caracteres en pwd 5, caracteres en posición correcta: 0
#
# CLAVE: 11223
# CODIG: 15633  PISTAS: caracteres en pwd 2, caracteres en posición correcta: 2
# CODIG: 11321  PISTAS: caracteres en pwd 4, caracteres en posición correcta: 3
#
# CLAVE: 11223
# CODIG: 1563341  PISTAS: caracteres en pwd 3, caracteres en posición correcta: 2
# CODIG: 11321ab  PISTAS: caracteres en pwd 4, caracteres en posición correcta: 3


class Clave:
    def __init__(self):
        self.code = ['C','H','R','I','S','1','3','0','2','3','0']
#        self.code = ['b','a','n','a','n','a']
        self.clue1 = 0 #caracteres en pwd
        self.clue2 = 0 #caracteres en posición correcta

    def reset_clues(self):
        self.clue1=0
        self.clue2=0

    def success(self):
        success_print()
        self.reset_clues()
        code = input(">> ")
        os.system('clear')

    def failure(self):
        failure_print(self.clue1,self.clue2)
        self.reset_clues()

    def check_code(self, code_list):
        if len(code_list)!=len(self.code):
            success = False
        else:
            success = True
            for i in range(len(self.code)):
                if code_list[i] != self.code[i]:
                    success = False
        return success

    def clues(self, code_list):
        copy_code = self.code.copy()
        for i in code_list:
            if i in copy_code:
                self.clue1+=1
                copy_code.remove(i)
        try:
            for i in range(len(self.code)):
                if code_list[i] == self.code[i]:
                    self.clue2+=1
        except IndexError:
            pass


def main():
    clave_x = Clave()
    os.system('clear')
    while True:
        # os.system('clear')
        #print (header)
        code = input("INTRODUZCA CÓDIGO>> ")
        code_list= split_code(code)

        if clave_x.check_code(code_list):
            clave_x.success()
        else:
            clave_x.clues(code_list)
            clave_x.failure()

if __name__ == "__main__":
    while True:
        try:
            main()
        except KeyboardInterrupt:
            sys.stdout.write("\nDECODE EXIT\n")
            sys.exit()
